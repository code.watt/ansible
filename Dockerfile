FROM python:3.9.0-buster

RUN echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu bionic main" >> /etc/apt/sources.list && \
	apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367 && \
	apt update && \
	apt install ansible -y && \
	pip3 --no-cache-dir install ansible-lint yamllint && \
	ansible-galaxy collection install community.general && \
	rm -rf /var/lib/apt/lists/*

CMD ["ansible"]